package com.amsal.ta.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AppResult implements Parcelable {
    public AppInfo app;
    public String apkName;
    public String status;
    public String malware;
    public String detail;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public AppInfo getApp() {
        return app;
    }

    public void setApp(AppInfo app) {
        this.app = app;
    }

    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMalware() {
        return malware;
    }

    public void setMalware(String malware) {
        this.malware = malware;
    }

    public static Creator<AppResult> getCREATOR() {
        return CREATOR;
    }

    public AppResult(Parcel in) {
        app = in.readParcelable(AppInfo.class.getClassLoader());
        apkName = in.readString();
        status = in.readString();
        malware = in.readString();
        detail = in.readString();
    }

    public static final Creator<AppResult> CREATOR = new Creator<AppResult>() {
        @Override
        public AppResult createFromParcel(Parcel in) {
            return new AppResult(in);
        }

        @Override
        public AppResult[] newArray(int size) {
            return new AppResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(app, flags);
        dest.writeString(apkName);
        dest.writeString(status);
        dest.writeString(malware);
        dest.writeString(detail);
    }

    public AppResult() {

    }
}
