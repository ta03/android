package com.amsal.ta.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amsal.ta.DetailResultActivity;
import com.amsal.ta.R;
import com.amsal.ta.ResultActivity;
import com.amsal.ta.model.AppResult;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.CardViewViewHolder> {
    private ArrayList<AppResult> listAppResult;
    private PackageManager packageManager;
    private Context context;
    private AppAdapter.OnItemClickCallback onItemClickCallback;

    public ResultAdapter(@NonNull Context context, ArrayList<AppResult> list) {
        this.listAppResult = list;
        packageManager = context.getPackageManager();
        this.context = context;
        this.listAppResult = list;
    }

    public void setOnItemClickCallback(AppAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.result_list, viewGroup, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardViewViewHolder holder, int position) {

        AppResult result = listAppResult.get(position);
        Drawable background = result.getApp().info.loadIcon(packageManager);
        holder.appPhoto.setBackgroundDrawable(background);
        if (result.getStatus().equals("NOT DANGEROUS")) {
            holder.tvStatus.setText(result.getStatus());
            holder.tvStatus.setTextColor(Color.GREEN);
        } else {
            holder.tvStatus.setTextColor(Color.RED);
            holder.tvStatus.setText(result.getStatus());
        }
        holder.tvName.setText(result.getApkName());
        holder.tvMalware.setText(result.getMalware());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailResultActivity.class);
                intent.putExtra(DetailResultActivity.DETAIL_APP, result);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listAppResult.size();
    }

    class CardViewViewHolder extends RecyclerView.ViewHolder {
        ImageView appPhoto;
        TextView tvName, tvMalware, tvStatus;
        Button btnDetail;

        CardViewViewHolder(View itemView) {
            super(itemView);
            appPhoto = itemView.findViewById(R.id.img_item_photo);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvMalware = itemView.findViewById(R.id.tv_item_malware);
            tvStatus = itemView.findViewById(R.id.tv_item_status);
            btnDetail = itemView.findViewById(R.id.btn_set_detail);
        }
    }
}