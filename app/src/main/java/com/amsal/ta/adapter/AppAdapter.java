package com.amsal.ta.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amsal.ta.R;
import com.amsal.ta.model.AppInfo;

import java.util.ArrayList;
import java.util.List;

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.GridViewHolder> {
    private LayoutInflater layoutInflater;
    private PackageManager packageManager;
    private List<AppInfo> apps;


    private OnItemClickCallback onItemClickCallback;

    public AppAdapter(@NonNull Context context, List<AppInfo> apps) {
        layoutInflater = LayoutInflater.from(context);
        packageManager = context.getPackageManager();
        this.apps = apps;
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_layout, viewGroup, false);
        return new GridViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final GridViewHolder holder, int position) {
        final AppInfo current = apps.get(position);

        holder.textViewTitle.setText(current.label);
        Drawable background = current.info.loadIcon(packageManager);
        holder.imageView.setBackgroundDrawable(background);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current.setChecked(!current.isChecked());
                holder.check.setVisibility(current.isChecked() ? View.VISIBLE : View.GONE);
                onItemClickCallback.onItemClicked(apps.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }
    public class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewTitle;
        private ImageView check;
        public GridViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.icon_image);
            textViewTitle = itemView.findViewById(R.id.titleTextView);
            check = itemView.findViewById(R.id.imageCheck);
        }
    }
    public interface OnItemClickCallback {
        void onItemClicked(AppInfo data);
    }
    public int getSelected() {
        int count = 0;
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).isChecked()) {
                count++;
            }
        }
        return count;
    }
    public void clearSelected() {
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).isChecked()) {
                apps.get(i).setChecked(false);
            }
        }
    }

    public ArrayList<AppInfo> getSelectedApk(){
        ArrayList<AppInfo> listApps = new ArrayList<AppInfo>();
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).isChecked()) {
                listApps.add(apps.get(i));
            }
        }
        return listApps;
    }
}
