package com.amsal.ta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashscreenActivity extends AppCompatActivity {

    Handler handler;
    Animation topAnim;
    Animation botAnim;
    ImageView image, logo1, logo2;
    TextView logo, slogan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splashscreen);

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        botAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);

        image = findViewById(R.id.logo);
        logo1 = findViewById(R.id.LogoDel);
        logo2 = findViewById(R.id.LogoHimatif);
        logo = findViewById(R.id.txt_apps_name);
        slogan = findViewById(R.id.txt_apps);

        image.setAnimation(topAnim);
        logo1.setAnimation(botAnim);
        logo2.setAnimation(botAnim);
        logo.setAnimation(botAnim);
        slogan.setAnimation(botAnim);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent moveIntent = new Intent(SplashscreenActivity.this, IntroActivity.class);
                startActivity(moveIntent);
                finish();
            }
        }, 4000);

    }
}
