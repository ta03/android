package com.amsal.ta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amsal.ta.model.AppResult;

public class DetailResultActivity extends AppCompatActivity {

    public static final String DETAIL_APP = "detail_app";
    private ImageView logo;
    private TextView appName, appStatus, appMalware, appKeterangan;
    private CardView appCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_result);
        logo = findViewById(R.id.app_logo);
        appName = findViewById(R.id.app_name);
        appStatus = findViewById(R.id.status);
        appMalware = findViewById(R.id.malware_name);
        appCard = findViewById(R.id.profileCard);
        appKeterangan = findViewById(R.id.detail);

        AppResult app = getIntent().getParcelableExtra(DETAIL_APP);

        Drawable background = app.getApp().info.loadIcon(getPackageManager());
        logo.setBackgroundDrawable(background);

        appName.setText(app.getApkName());
        appMalware.setText(app.getMalware());
        appKeterangan.setText(app.getDetail());
        appStatus.setText(app.getStatus());
        if (app.getStatus().equals("NOT DANGEROUS")) {
            appStatus.setTextColor(Color.GREEN);
        } else {
            appStatus.setTextColor(Color.RED);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
